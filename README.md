# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : [midterm_modified_Lab06form]
* Key functions (add/delete)
    [index.html]
    1. Navigation Bar Functions
        A. Home Button (Forum button)
            a. Refresh index.html
        B. Login/Profile (Account button)
            a. Move to signin.html for sign in
            b. When signed in, it is able to access user profile,
            by clicking on user email
            c. When signed in, logout option is available
        C. Page Switch (Default: News)
            a. Switch page to show different post lists
        D. Create New Post
            a. If not signed in, link to signin.html
            b. If signed in, show create_new_post form
        (E). Menu button
            a. When window width < 767.98px,
               hide other navigation bar item
               Click button to hide or display
            b. Selecting function will collapse the menu box
    2. Post List Field Function
        A. End to Head Listing
            a. To put newer post to the top of the list,
               put list element from end to head
        B. Open post content
            a. Click on post title will hide post list,
               and display post title, content and comment form
    3. Create_New_Post Form function
        A. Post to Current Page
            a. Initially, post category is set to current page
               But can manual switch to desired category
        B. Text Value Security Check
            a. To avoid HTML code being corrupted by text content,
               character '<' and '>' are prohibited
            b. Containing any of these characters will
               give a warning alert at the top of the page
        C. Submit Button
            a. After submitting, page will load to post page
               so user can see their post being updated on the list
        D. Cancel Button
            a. Close the create_new_post form and
               display current page post list
    4. Post Content Function
        A. Text Value Security Check
           (As same as [index.html].3.B)
        B. Regular Listing
            a. Newer comments are shown at the end of the list
        C. Submit Button
            a. Push comment field text and user email to comment list
        D. Exit Post Button
            a. Quit current post and return to previous page
    [signin.html]
    1. Sign In Button
        A. Sign in with custom account
    2. Sign In with Google Button
        A. Sign in with Google Popup
    3. New Account Button
        A. Link to register.html
    [register.html]
    1. New Account Button
        A. Sign up New Account
    [user.html]
    1. User Name Animation Display
        A. Display user name over icon
    2. User Email Display
        A. display user Email
    3. Return Button
        A. Return to index.html
    
* Other functions (add/delete)
    [index.html] text_chk("text"): Text value security check
    [index.html] re_list(list): End to head listing

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|N|
|Firebase Page|5%|N|
|Database|15%|N|
|RWD|15%|N|
|Topic Key Function|15%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|

## Website Detail Description

# 作品網址：[https://midtermproject-90af1.firebaseapp.com]

# Components Description : 
[index.html]
1. Navigation Bar Functions
    A. Home Button (Forum button)
        a. Use a click event listener to refresh index.html.
           *An intuitive option for user to return to home page.
    B. Login/Profile (Account button)
        a. Use a click event listener to link to signin.html.
        b. Use onAuthStateChanged to update user state
        c. Change innerHTML to modify dropdown element
    C. Page Switch (Default: News)
        a. Assign click event listener to change current Page
    D. Create New Post
        a. Assign click event listener to detect click
        b. then use onAuthStateChanged to determine to
           redirect to signin.html or open create_new_post form.
    (E). Menu button
        a. Use @media, bootstrap to apply RWD.
        b. Use jQuery to toggle dropdown.
2. Post List Field Function
    A. End to Head Listing
        a. (Read other functions ["index.html"].1)
    B. Open post content
        a. Assign click event listener to post title,
           then tarverse its sibling and child to acquire
           hidden post key.
           After that, use post key to load desired data
           from database.
3. Create_New_Post Form function
    A. Post to Current Page
        a. Set post destination to current page by using innerHTML,
           then add click event listener to page options.
    B. Text Value Security Check
        a. (Read other functions ["index.html"].2)
    C. Submit Button
        a. Use click event listener and database reference
           to push data.
    D. Cancel Button
        a. Use click event listener to change style.display value
           and relisting posts.
4. Post Content Function
    A. Text Value Security Check
        (Read other functions ["index.html"].2)
    B. Regular Listing
        a. Save data in array and assign it to innerHTML by join(' ').
    C. Submit Button
        a. Use click listener to detect and use auth to get
           current user data.
    D. Exit Post Button
        a. Use click listener to change style.display value to
           hide post and re_list() to list page posts.

# Other Functions Description(1~10%) : 
[index.html]
1. re_list:
   Get list length, then concatenate the last element to first element as a string.
   *The String is used as innerHTML for post listing.
2. text_chk:
   Get text length, check character one by one; if any '<' or '>' is found
   the result is false. Otherwise, the return value is true.
   *It is use to filter post content and comments which directly changes innerHTML.

## Security Report (Optional)
1. Input text character filtering
    function: text_chk(text);
    The function is executed before pushing text value of textboxes,
    which will be used for innerHTML later on. The function banned the existence of '<' and '>',
    thus stops HTML corruption cause by using HTML tags.

    Case: Post listing sets innerHTML with database snapshot's title, userEmail and content.
