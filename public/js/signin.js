function initApp() {
	var txtEmail = document.getElementById("inputEmail");
	var txtPassword = document.getElementById("inputPassword");

	// Login with Email/Password
	var btnLogin = document.getElementById("btnLogin");
	btnLogin.addEventListener('click', function () {
		/// TODO 2: Add email login button event
		///         1. Get user input email and password to login
		///         2. Back to index.html when login success
		///         3. Show error message by "create_alert" and clean input field
		firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value)
		.then(function() {
			txtEmail.value = "";
			txtPassword.value = "";
			create_alert("success", "Login Success!");
			window.location.href = "index.html";
		})
		.catch(function(error) {
			create_alert("error","Login Failed!");
		});
	});

	var btnGoogle = document.getElementById("btngoogle");
	btnGoogle.addEventListener('click', function () {
		/// TODO 3: Add google login button event
		///         1. Use popup function to login google
		///         2. Back to index.html when login success
		///         3. Show error message by "create_alert"
		var provider = new firebase.auth.GoogleAuthProvider();
		firebase.auth().signInWithPopup(provider).then(function(result) {
			// This gives you a Google Access Token. You can use it to access the Google API.
			var token = result.credential.accessToken;
			// The signed-in user info.
			var user = result.user;
			// ...
			create_alert("success", "Login Success!");
			window.location.href = "index.html";
		}).catch(function(error) {
			// Handle Errors here.
			var errorCode = error.code;
			var errorMessage = error.message;
			// The email of the user's account used.
			var email = error.email;
			// The firebase.auth.AuthCredential type that was used.
			var credential = error.credential;
			// ...
			create_alert("error","Login Failed!");
		});
	});
}

window.onload = function () {
	initApp();
};