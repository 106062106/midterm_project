function re_list(list_ctg) {
  var postString = "";
  var last = list_ctg.length;
  if (last == 0) {
    postString = "No post found.";
  } else {
    for (var i = last - 1; i >= 0; i--) {
      postString = postString + list_ctg[i] + ' ';
    }
  }
  document.getElementById("post_area").innerHTML = postString;
}

function re_cmt(list_ctg) {
  document.getElementById("comment_area").innerHTML = list_ctg.join(' ');
}

function text_chk(text) {
  var len = text.length;
  for (var i = 0; i < len; i++) {
    switch(text[i]) {
      case "<":
      case ">":
        return false;
      case "default":
        break;
    }
  }
  return true;
}

function init() {
  var user_email = '';
  firebase.auth().onAuthStateChanged(function (user) {
    var menu = document.getElementById('dynamic-menu');
    // Check user login
    if (user) {
      user_email = user.email;
      menu.innerHTML = "<a class='dropdown-item' href='user.html'>" + user.email + "</a><span class='dropdown-item' id='logout-btn'>Logout</span>";
      /// TODO 5: Complete logout button event
      ///         1. Add a listener to logout button
      ///         2. Show alert when logout success or error (use "then & catch" syntex)
      var logOutButton = document.getElementById('logout-btn');
      logOutButton.addEventListener('click', function()
      {
        firebase.auth().signOut().then(function() {
        	// Sign-out successful.
          create_alert("success", "logOutSucceed");
          window.location.href="index.html";
      	}).catch(function(error) {
        	create_alert("error", error.message);
      	})
    	})
    } else {
      // It won't show any post if not login
      menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
    }
  });

  var topPage = document.getElementById("dropdown02");
  var pgNew = document.getElementById("pg_new");
  var pgEnt = document.getElementById("pg_ent");

  var postForm = document.getElementById("post_form");
  var postArea = document.getElementById("post_area");
  var visitPost = document.getElementById("visit_post");

  var newsPosts = [];
  var entsPosts = [];

  pgNew.addEventListener('click', function() {
    topPage.innerHTML = pgNew.innerHTML;
    postForm.style.display = "none";
    postArea.style.display = "block";
    visitPost.style.display = "none";
    re_list(newsPosts);
  });
  pgEnt.addEventListener('click', function() {
    topPage.innerHTML = pgEnt.innerHTML;
    postForm.style.display = "none";
    postArea.style.display = "block";
    visitPost.style.display = "none";
    re_list(entsPosts);
  })

	var btnNewPost = document.getElementById("new_post");
	btnNewPost.addEventListener('click', function() {
    // Check user state
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        var postCtg = document.getElementById("post_ctg");
        postForm.style.display = "block";
        postArea.style.display = "none";
        visitPost.style.display = "none";
        postCtg.innerHTML = topPage.innerHTML;
      } else {
        window.location.href = "signin.html";
      }
    })
  });

  var vstKey = document.getElementById("vst_key");
  var cmtArea = document.getElementById("comment_area");
  var cmtList = [];

  postArea.addEventListener('click', function(e) {
    if (e.target && e.target.matches("h6#posts")) {
      var key = e.target.children[0].innerHTML;
      var path = topPage.innerHTML + '/' + key;
      
      postForm.style.display = "none";
      postArea.style.display = "none";
      visitPost.style.display = "block";

      var vstTtl = document.getElementById("vst_ttl");
      var vstEml = document.getElementById("vst_eml");
      var vstCnt = document.getElementById("vst_cnt");
      firebase.database().ref(path).once('value').then(function(snapshot) {
        vstKey.innerHTML = key;
        vstTtl.innerHTML = snapshot.val()['title'];
        vstEml.innerHTML = snapshot.val()['userEmail'];
        vstCnt.innerHTML = snapshot.val()['content'];
      });

      cmtArea.innerHTML = "";
      var cmtRef = firebase.database().ref(path);
      cmtRef.on('child_added', function(added_child) {
        if (added_child.val()['userEmail']) {
          var bfrCmtEml = "<div><h6>"
          var bfrCmtCtn = "</h6><p>"
          var endCmtCtn = "</p>"
          cmtList.push(bfrCmtEml + added_child.val()['userEmail'] + bfrCmtCtn + added_child.val()['comment'] + endCmtCtn);
          re_cmt(cmtList);
        }
      })
    }
  });

  var postTtl = document.getElementById("post_ttl");
  var postCtg = document.getElementById("post_ctg");
  var postCnt = document.getElementById("content");

  var btnCtgNew = document.getElementById("ctg_new");
  btnCtgNew.addEventListener('click', function() {
    postCtg.innerHTML = btnCtgNew.innerHTML;
  });
  var btnCtgEnt = document.getElementById("ctg_ent");
  btnCtgEnt.addEventListener('click', function() {
    postCtg.innerHTML = btnCtgEnt.innerHTML;
  });

  var btnPost = document.getElementById("post_btn");
  btnPost.addEventListener('click', function() {
    if (postCnt.value != "") {
      var user = firebase.auth().currentUser;
      var postRef = firebase.database().ref(postCtg.innerHTML);
      if (!text_chk(postCnt.value) || !text_chk(postTtl.value)) {
        create_alert("error", "Text contained prohibited character: '<', '>', ...")
        return;
      }
      postRef.push({
        title: postTtl.value,
        userEmail: user.email,
        content: postCnt.value
      });
      topPage.innerHTML = postCtg.innerHTML;
      postTtl.value = "";
      postCnt.value = "";
      postForm.style.display = "none";
      postArea.style.display = "block";
      visitPost.style.display = "none";
      switch (topPage.innerHTML) {
      case "News":
        re_list(newsPosts);
        break;
      case "Entertainment":
        re_list(entsPosts);
        break;
      default:
      }
    }
  });

  var btnBack = document.getElementById("back_btn");
  btnBack.addEventListener('click', function() {
    postForm.style.display = "none";
    postArea.style.display = "block";
    visitPost.style.display = "none";
  });

  var str_before_key = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 id='posts' class='border-bottom border-gray pb-2 mb-0' style='width:90%; overflow:hidden;'><span style='display:none'>";
  var str_before_username = "</h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><div class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray' style='width:50%; overflow:hidden;'><strong class='d-block text-gray-dark' style='width:50%; overflow:hidden;'>";
  var str_after_content = "</div></div></div>\n";

  var newsRef = firebase.database().ref("News");
  newsRef.on('child_added', function(added_child) {
    newsPosts.push(str_before_key + added_child.key + "</span>" + added_child.val()['title'] + str_before_username + added_child.val()['userEmail'] + "</strong>" + added_child.val()['content'] + str_after_content);
    if (topPage.innerHTML == "News") {
      re_list(newsPosts);
    }
  });
  var entsRef = firebase.database().ref("Entertainment");
  entsRef.on('child_added', function(added_child) {
    entsPosts.push(str_before_key + added_child.key + "</span>" + added_child.val()['title'] + str_before_username + added_child.val()['userEmail'] + "</strong>" + added_child.val()['content'] + str_after_content);
    if (topPage.innerHTML == "Entertainment") {
      re_list(entsPosts);
    }
  });

  var txtCmt = document.getElementById("comment");
  var btnCmt = document.getElementById("cmt_btn");
  var btnRtn = document.getElementById("rtn_btn");
  btnCmt.addEventListener('click', function() {
    var path = topPage.innerHTML + '/' + vstKey.innerHTML;
    var cmtRef = firebase.database().ref(path);
    var user = firebase.auth().currentUser;
    if (!text_chk(txtCmt.value)) {
      create_alert("error", "Text contained prohibited character: '<', '>', ...")
      return;
    }
    cmtRef.push({
      userEmail: user.email,
      comment: txtCmt.value
    });
    txtCmt.value = "";
  });
  btnRtn.addEventListener('click', function() {
    postForm.style.display = "none";
    postArea.style.display = "block";
    visitPost.style.display = "none";
  });

  re_list(newsPosts);
}

window.onload = function () {
  init();
  // Chrome notification
  var notifyConfig = {
    body: '\\ ^o^ /',
    icon: '/img/notify.png'
  };
    
  if (Notification.permission === 'default' || Notification.permission === 'undefined') {
    Notification.requestPermission(function (permission) {
      if (permission === 'granted') {
        var notification = new Notification('Notification Active!!', notifyConfig);
      }
    });
  } else {
    console.log("Notification permission denied");
  }
};