function initApp() {
	// Login with Email/Password
	var regNam = document.getElementById('reg_nam');
	var regEml = document.getElementById('reg_eml');
	var regPwd = document.getElementById('reg_pwd');
	var btnSignUp = document.getElementById('btnSignUp');

	firebase.auth().onAuthStateChanged(function(check) {
		if (check) {
			var user = firebase.auth().currentUser;
			user.updateProfile({
				displayName: regNam.value
			}).then(function() {
				create_alert("success", "DisplayName updated!");
				window.location.href = "index.html";
			}).catch(function() {
				create_alert("failed", "DisplayName unaffected!")
			});
		}
	});

	btnSignUp.addEventListener('click', function () {        
			/// TODO 4: Add signup button event
			///         1. Get user input email and password to signup
			///         2. Show success message by "create_alert" and clean input field
			///         3. Show error message by "create_alert" and clean input field
			firebase.auth().createUserWithEmailAndPassword(regEml.value, regPwd.value)
			.then(function() {
				create_alert("success", "Login Success!");
			})
			.catch(function(error) {
					create_alert("error","Login Failed!");
			});
	});
}

// Custom alert
function create_alert(type, message) {
	var alertarea = document.getElementById('custom-alert');
	if (type == "success") {
			str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
			alertarea.innerHTML = str_html;
	} else if (type == "error") {
			str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
			alertarea.innerHTML = str_html;
	}
}

window.onload = function () {
	initApp();
};