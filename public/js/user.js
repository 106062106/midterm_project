function initApp() {
	var user = firebase.auth().currentUser;
	var usr_nam = document.getElementById("usr_nam");
	var usr_eml = document.getElementById("usr_eml");

	firebase.auth().onAuthStateChanged(function (user) {
		usr_nam.innerHTML = user.displayName;
    usr_eml.innerHTML = user.email;
	});
}

window.onload = function () {
	initApp();
};