// Initialize Firebase
/// TODO 1: Change to your firebase config
///         1. New a Firebase project
///         2. Copy and paste firebase config below
var config = {
  apiKey: "AIzaSyBNe6TPg1dmnFkPAlY5ngM6d0RsACJXOZI",
  authDomain: "midtermproject-90af1.firebaseapp.com",
  databaseURL: "https://midtermproject-90af1.firebaseio.com",
  projectId: "midtermproject-90af1",
  storageBucket: "midtermproject-90af1.appspot.com",
  messagingSenderId: "897210326278"
};
firebase.initializeApp(config);

function create_alert(type, message) {
	var alertarea = document.getElementById('custom-alert');
	if (type == "success") {
		str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
		alertarea.innerHTML = str_html;
	} else if (type == "error") {
		str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
		alertarea.innerHTML = str_html;
	}
}